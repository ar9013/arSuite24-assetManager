package com.yue.ar.suite;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class ARSuite extends ApplicationAdapter {
	SpriteBatch batch;

	private static final String BADLOGIC = "badlogic.jpg";
	private static final String LOGO = "pepsi.png";

	private Texture badlogicTexture;
	private Texture logoTexture;

	AssetManager assetManager;

	// 資源在 assets 文件夾中的相對路徑

	@Override
	public void create () {
		batch = new SpriteBatch();

		// 建立資源管理器
		assetManager = new AssetManager();

		// 加載資源
		assetManager.load(BADLOGIC,Texture.class);
		assetManager.load(LOGO,Texture.class);

		// 阻塞線程，等待資源加載完畢
		assetManager.finishLoading();


		// 加载完毕后获取资源实例
		badlogicTexture = assetManager.get(BADLOGIC, Texture.class);
		logoTexture = assetManager.get(LOGO, Texture.class);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.begin();
		batch.draw(badlogicTexture,0,0);
		batch.draw(logoTexture, 10, 300);
		batch.end();
	}
	
	@Override
	public void dispose () {

		if(assetManager != null){
			  /*
             * 释放所有资源, 并销毁 AssetManager。
             *
             * 注意: 本例中的 Texture 通过 AssetManager 进行管理,
             * 因此这里不需要再调用 Texture 的 dispose() 方法。
             */
			assetManager.dispose();
		}

	}
}
